import React from 'react';






const CountryDropdown = ({ countrylist,onCountrySelect }) => {

    
    return (
        <div className="field">
            <label>Country</label>
            <select id="countryselect" required onChange={onCountrySelect}>
            <option value="" key="default">Select a Country</option>
                {countrylist.map((country) =>(
                    <option value={country.code} key={country.code}>{country.name}</option>
                ))}
            </select>
        </div>
    );
};

export default CountryDropdown;
