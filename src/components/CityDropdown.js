import React from 'react';






const CityDropdown = ({ citylist,onCitySelect }) => {

    
    return (
        <div className="field">
            <label>City</label>
            <select id="cityselect" required onChange={onCitySelect}>
            <option value="" key="default">Select a City</option>
                {citylist.map((city) =>(
                    <option value={city.name} key={city.name}>{city.name}</option>
                ))}
            </select>
        </div>
    );
};

export default CityDropdown;
