import React from 'react';
import { Bar } from "react-chartjs-2";


const renderContent =(data,options) => {
    if ((data['labels']).length === 0 && (data['datasets']).length === 0){
        return(<div></div>);
    }
    else{
        return(<div>
                    <h4>Graphical Data</h4><hr/>
                    <Bar
                        data={data}
                        width={null}
                        height={null}
                        options={options}
                    />
            </div>);
    }
    
}






const Graph = ({ data }) => {

    const options = {
        responsive: true,
        legend: {
          display: false
        },
        type: "bar"
    };

    return (
        <div className="field">
            
            {renderContent(data,options)}
        </div>
    );
};

export default Graph;
