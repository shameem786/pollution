import React from 'react';
import siteapi from '../apis/siteapi';   //api call 
import CountryDropdown from './CountryDropdown';
import CityDropdown from './CityDropdown';
import Graph from './Graph';


class App extends React.Component {

    state={ countries:[], cities:[], selectedCountry:'', selectedCity:'', DateFrom:'', DateTo:'', 
    data:{labels:[],datasets:[]} }

    componentDidMount(){
        this.onPageLoad();   //calling api for getting country list when page is loading
    }

    //function to call api for getting all countries   
    onPageLoad =async () => {
        const response = await siteapi.get('/countries',{  
        });
        //console.log(response['data']['results']);
        this.setState({ 
            countries:response['data']['results'] 
        });
        //console.log(this.state.countries);
    };

    //call back function, calling from  CountryDropdown.js
    onCountrySelect = (e) =>{
        this.setState({selectedCountry:e.target.value},()=>{
            if (this.state.selectedCountry) {
                this.cityListCalling();  //function for calling city api  
                //console.log(this.state.selectedCountry);
              } 
        });   
    }

    //function to call api for getting all cities in a perticula country   
    cityListCalling =async () => {
        let country_code=this.state.selectedCountry;
        //console.log(this.state.selectedCountry);
        const response = await siteapi.get('/cities',{
            params:{
                country:country_code
            }
        });
        //console.log(response);
        this.setState({ 
            cities:response['data']['results'] 
        });
        //console.log(this.state.cities);
    };

    //call back function, calling from  CityDropdown.js
    onCitySelect = (e) =>{
        this.setState({selectedCity:e.target.value},()=>{
            if (this.state.selectedCity) {
                //this.cityListCalling();  
                //console.log(this.state.selectedCity);
              } 
        });
        
    }

    //onchange in date from
    FromDate=(e) => {
        
        let selected_date=new Date(e.target.value)
        let  today = new Date()
        //console.log(today);
        if(today >= selected_date){      //date picking validations
            if(this.state.DateTo){   //comparing date(to) with date(from)
                let to_date=new Date(this.state.DateTo);
                if(selected_date <= to_date){
                    this.setState({ DateFrom:e.target.value});
                }
                else{
                    alert("You can't select a date succeeding  'date(To)'");
                }
            }
            else{
                this.setState({ DateFrom:e.target.value});
            }
        }
        else{
            alert("You can't select a date succeeding today's date");
        }
    }



    //onchange in date to
    ToDate=(e) => {
        let selected_date=new Date(e.target.value)
        let  today = new Date()
        //console.log(today);
        if(today >= selected_date){      //date picking validations
            if(this.state.DateFrom){   //comparing date(to) with date(from)
                let from_date=new Date(this.state.DateFrom);
                if(selected_date >= from_date){
                    this.setState({ DateTo:e.target.value});
                }
                else{
                    alert("You can't select a date preceeding  'date(From)'");
                }
            }
            else{
                this.setState({ DateTo:e.target.value});
            }
        }
        else{
            alert("You can't select a date succeeding today's date");
        }
    }


    //form submitting function
    onFormSubmit=async (e) => {
        e.preventDefault();
        
        const response = await siteapi.get('/measurements',{
            params:{
                country:this.state.selectedCountr,
                city:this.state.selectedCity,
                date_from:this.state.DateFrom,
                date_to:this.state.DateTo
            }
        });
        //console.log(response);
        const   results=response.data.results;
        const o3=[]; //for storing o3 values
        const label=[]; //for storing labels
        const pm25=[]; //for storing pm25 values
        results.map((result) => {
                if(result.parameter==='o3'){
                label.push(result.date.local);
                o3.push(result.value);
        }
        else{//case of pm25
            pm25.push(result.value);
        }
        })
        
        //console.log('det',pm25);
        //console.log('det2',o3);
        //console.log('label',label);
        if(o3 && pm25){
            this.setState({ 
                data: {
                     labels:label,
                     datasets: [
                         /*
                       {
                         label: "o3",
                         backgroundColor: "rgba(255,99,132,0.2)",
                         borderColor: "rgba(255,99,132,1)",
                         borderWidth: 1,
                         //stack: 1,
                         hoverBackgroundColor: "rgba(255,99,132,0.4)",
                         hoverBorderColor: "rgba(255,99,132,1)",
                         data:o3
                        
                       },*/
                           {
                         label: "pm25",
                         backgroundColor: "rgba(155,231,91,0.2)",
                         borderColor: "rgba(245, 229, 27, 1)",
                         borderWidth: 1,
                         //stack: 1,
                         hoverBackgroundColor: "rgba(255,99,132,0.4)",
                         hoverBorderColor: "rgba(255,99,132,1)",
                         data: pm25
                       } 
                     ]
                   } 
             });
        
           }
    }



    
    render(){
        return(
            <div className="ui container">
                <h1><center>Pollution Data</center></h1><hr />
                <div>
                    <form onSubmit={this.onFormSubmit} className="ui form">
                            <div className="two fields">
                                <CountryDropdown  countrylist={this.state.countries}  onCountrySelect={this.onCountrySelect} />
                                <CityDropdown  citylist={this.state.cities}  onCitySelect={this.onCitySelect} />
                            </div>
                        
                            
                            
                            <div className="two fields">
                                <div className="field">
                                    <label>Date(From)</label>
                                    <input type="date" onChange={this.FromDate} value={this.state.DateFrom} required/>
                                </div>
                                <div className="field">
                                    <label>Date(To)</label>
                                    <input type="date" onChange={this.ToDate} value={this.state.DateTo} required />
                                </div>
                            </div>
                            

                            <div >
                                    
                                    <input type="submit" value="Show Graph" className="ui submit primary button" />
                            </div>

                    </form>
                </div>
                <br />
                <div>
                    <div className="ui one column grid">
                        
                        <div className="column">

                            <Graph data={this.state.data} />
                            
                        </div>

                    </div>
                </div>

                
            </div>
        );
    }
}



export default App;


